---
layout: default
title: FUI UV boost
subtitle: UV boosting (Patent)
---

### Plant defense stimulation to biotic stress throught light treatments

- Post-doctorate 2015-2016
- Institutes:
  - University UAPV, Avignon, France
  - Claranor, Avignon, France
  - Univeristy, Montpellier, France

<div class="logos">
    <ul>
        <li><img src="/assets/img/logo/uapv.png" alt="logo UAPV" /></li>
        <li><img src="/assets/img/logo/claranor.png" alt="logo CLARANOR" /></li>
        <li><img src="/assets/img/logo/lirmm.png" alt="logo LIRMM" /></li>
   </ul>
</div>


<div class="mosaic">
    <img src="/assets/img/claranor/test_patho_small.jpg" class ="wide"/>
    <img src="/assets/img/claranor/handy_pea_pince_small.jpg" />
    <img src="/assets/img/claranor/infection_tige_small.jpg" />
    <img src="/assets/img/claranor/vine_small.jpg" class ="wide" />
</div>


**Team members & co-workers**

- Urban Laurent (UAPV)
- Aarrouf Jawad (UAPV)
- Chabane Sari Dounyazade (SATT Sud-Est - UM)
- Orsal Bernard (LIRMM - UM)

### Abstract

Due to patent confidentiality, no informations are available here.

Sorry for the inconvenience.

Start-up based on part of this work: <a href="https://uvboosting.com/" target="_blank">Start-up</a>

Keywords <mark>Vitis vinifera</mark> <mark>Lactuca sativa</mark> <mark>Solanum lycopersicum</mark> <mark>Capsicum annuum</mark> <mark>Pulsed light</mark> <mark>Ecophysiology</mark> <mark>Plant defense</mark> <mark>Organic farming</mark> <mark>Data Analysis</mark> <mark>Chlorophyll _a_ fluorescence</mark> <mark>Abiotic and Biotic Stress</mark>
### References

Information available in press: <a href="https://technofounders.com/actualite/uv-boosting-devoile-les-resultats-dessais-de-la-saison-2020-et-confirme-son-efficacite/" target="_blank">Article</a>
