
- Ripoll J., Relier S., Bastide A., David A., Rivals E. **2020**. Comparative transcriptomics upon shutdown of a major player in human epitranscriptome regulation. JOBIM 2020. **Montpellier, France** (30th June - 2th July). Digital poster

- Ripoll J., Bertin N., Urban L. **2015**. Parameters derived from fluorescence induction curves are powerful tools for characterizing plant adaptive strategies to water deficit according to genotype and developmental stage. DROPS Congress, **Montpellier, France** (June 8-9). <a href="https://hal.inrae.fr/hal-02797706" target="_blank">DOI</a>

- Ripoll J., Urban L., Brunel B., L'Hôtel J-C., Bertin N. **2014**. Impact of water deficit on tomato fruit growth and quality depends on the fruit developmental stage. 29e International Horticultural Congress, **Brisbane, Australia** (August 17-22). Digital poster <a href="https://hal.inrae.fr/hal-02742446" target="_blank">DOI</a>

- Ripoll J., Bertin N., Al Halabi R., Buatois B., Staudt M. **2014**. Monitoring plant odors in tomato culture for in-situ stress detection. Plant Biology Europe FESPB/EPSO Congress, **Dublin, Irland** (June 22-26). <a href="https://hal.inrae.fr/hal-02801046" target="_blank">DOI</a>

- Ripoll J., Urban L., Brunel B., Goujon A., L'Hôtel J-C., Causse M., Bertin N. **2014**. Variability of the genetic response of tomato during soil drying and repeated cycles of water deficit and recovery. Plant Biology Europe FESPB/EPSO Congress, **Dublin, Irland** (June 22-26). <a href="https://hal.inrae.fr/hal-02795616" target="_blank">DOI</a>

- Ripoll J., Urban L., Brunel B., Goujon A., L'Hôtel J-C., Causse M., Bertin N. **2014**. Combining deficit irrigation and recovery periods can save water and maintain yield and fruit quality in tomato. XVIIIth EUCARPIA Meeting of the Tomato Working Group, **Avignon, France** (April 22-25). _First prize for the best student poster_ <a href="https://hal.inrae.fr/hal-02743459" target="_blank">DOI</a>

- Ripoll J., Lecompte F., Nicot P., Lauri F., Urban L. and Bertin N. **2013**. How to improve Fruit Quality and Plant Resistance? - Impacts of Deficit Irrigation at the Plant and Fruit Scales. Journée Tersys, **Avignon, France** (September 12). _First prize for the best student poster_

- Ripoll J., Brunel B., L'Hôtel J-C., Bertin N., Urban L. **2012**. Adaptation des cultures au changement climatique : Réponse adaptative de la Tomate au déficit hydrique - Comparaison et sélection de différentes variétés commercialisables. Journée des Ecoles doctorales, **Avignon** (Oct 29-31).

- Ripoll J., Guermache F., Jeanneau M., Kashefi J., Coleman R., Briano J., Jones W.A., Strickman D., Bon M-C. **2010**. Contribution de la diversité génétique et de traits d'histoire de vie au pouvoir envahissant de la Morelle Jaune, Solanum elaeagnifolium Cav, dans un contexte de programme de lutte biologique. Colloque national Ecologie 2010, **Montpellier** (September 2-4).

- Beaudoin-Ollivier L., Frérot B., Ripoll J., et al. **2010**. Comparative activity of four Elaeidobius spp oil palm pollinators visiting oil palm inflorescences in Central Africa. Congress Palms 2010, International Symposium on the biology of the Palm family, **Montpellier** (May 5-7). <a href="https://agritrop.cirad.fr/578124/" target="_blank">DOI</a>
