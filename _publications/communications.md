
- Therizols G. et al. **2023**. Alteration of ribosome function upon 5-fluorouracil treatment
favors cancer cell drug-tolerance. JOBIM 2023, **Nice, France** (June 26-30). <a href="https://jobim2023.sciencesconf.org/data/pages/proceedings.pdf" target="_blank">DOI</a>

- Ripoll J. **2022**. Joint Transcriptome and Translatome Analysis: A Reproducible Pipeline and an Example in Cancer Research. Genotoul Biostat Bioinfo days. LIRMM, **INRAE, Auzeville, France** (30 Nov. 2022). <a href="https://bioinfo-biostat.sciencesconf.org/data/pages/genotoul_ripoll_2022.pdf" target="_blank">DOI</a>

- Ripoll J. **2022**. Snakemake & Workflow Hub (2022). Séminaire Bioinformatique MBI. **Genopolys, Montpellier, France** (24 Mar. 2022). <a href="https://gitlab.in2p3.fr/mbi/mbi/-/blob/master/20220106_RegistresPipelines/Workflow_Hub_mbi_ripoll.pdf" target="_blank">DOI</a>

- Ripoll J. **2019**. Understanding translational regulation processes in cancer cells. Doctorate and post-doctorate MAB days. LIRMM, **Montpellier, France** (24 Sept. 2019).

- Ripoll J. **2018**. Acclimatization of three coral species to ocean acidification around natural CO2 seeps: transplantation & gene expression. 4th APCRS congress. **Cebu, Philippines** (June 3-9).

- Ripoll J. **2017**. Complexité de l'analyse bioinformatique des données méta-génomiques/transcriptomiques. Séminaire «Les nouvelles techniques de génomiques: applications, contraintes et perspectives ». **University of New Caledonia, Noumea** (December 12).

- Ripoll J. **2016**. Parameters derived from fluorescence induction curves are powerful tools for characterizing water deficit effects on tomatoes according to genotypes and plant developmental stage. Umeä Plant Science Center, UPSC Days,Oral presentation as successful post-doc candidate (selected among 90 candidates), **Umeä, Sweden** (May 30-31).

- Ripoll J. **2014**. Les enjeux du déficit hydrique face aux changements climatiques : Améliorer la qualité des fruits et les défenses de la plante. Concours Ma thèse en 180 secondes, Finale locale d'**Avignon** (April 4).

- Ripoll J. **2013**. Effets de la contrainte hydrique, seule et en interaction avec d'autres stress biotique et abiotique, sur les mécanismes d'adaptation de la plante et la qualité du fruit, en fonction de la variabilité génétique. Journée Tersys 2013, **Avignon, France** (September 12).

- Ripoll J. **2010**. Contribution de la diversité génétique et des traits d'histoire de vie au pouvoir envahissant de la Morelle Jaune Solanum elaegnifolium Cavanilles (Solanacae) dans un contexte de programme de lutte biologique. Séminaire Printemps de Baillarguet 2010, **Montferrier sur lez, France** (April 29).
