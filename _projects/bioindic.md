---
layout: default
title: CNRT BioIndic
subtitle: Improving knowledge and practices - biological indicators
---

### Meta-analysis of metagenomic datasets on ultramafic soils of New Caledonia: towards a referential system in an ecological restoration context

- Post-doctorate 2018
- Institutes:
  - IAC, Noumea, New Caledonia

<div class="logos">
    <ul>
        <li><img src="/assets/img/logo/iac.png" alt="logo IAC" /></li>
    </ul>
</div>


<div class="mosaic">
    <img src="/assets/img/bioindic/bioindic_map.png" class="big" />
    <img src="/assets/img/bioindic/bioindic.jpg" />
    <img src="/assets/img/bioindic/bioindic_trajectory.png" />
</div>


**Team members & co-workers**

- Carriconde Fabian (IAC)
- Maggia Laurent (IAC)
- Fernandez-Nunez Nicolas (IAC)

### Part I: Potential of high-throughput eDNA sequencing of soil fungi and bacteria for monitoring ecological restoration in ultramafic substrates: The case study of the New Caledonian biodiversity hotspot

Due to their central role in ecosystems functioning and their ability to rapidly respond to environmental changes, soil microorganisms could potentially be used for monitoring ecosystems recovery in the context of degraded land restoration. However, these belowground organisms have been, to date, largely neglected. 

Here, we investigated fungal and bacterial community diversity, composition, and structure from ultramafic soils in New Caledonia, an archipelago in the southwest Pacific recognized as a priority for conservation and restoration. The emerging approach of high-throughput amplicon sequencing of environmental DNA (eDNA) – metabarcoding of eDNA – was used to compare soil microbial communities from four different native vegetation types, representing different stages of a chronosequence and defined as reference ecosystems, to five distinct post-mining sites revegetated several years ago. 

Our results clearly revealed changes in soil microbial phyla and functional groups along the reference chronosequence and variable responses at the different revegetated sites, with two of the five sites showing a good trajectory of recovery. We thus propose three ratios as metrics for monitoring the restoration trajectory of soil microorganisms: the Ascomycota:Basidiomycota and Saprotrophic:Ectomycorrhizal ratios for fungi, and the Cyanobacteria:Chloroflexi ratios for bacteria. 

Our study, combined with recent works undertaken in other geographical areas, underpins the great promise that could represent soil microbial eDNA metabarcoding for monitoring restoration progress and success. With the emergence of these new cost-effective and scalable sequencing technologies, soil microbes could, in the near future, be included in guidelines for restoration operations in complement to more conventional approaches.

### Part II: A meta-analysis of microbial metagenomic soil data from ultramafic soils: towards a soil microbial referential for conservation and restoration in the New Caledonian biodiversity hotspot ?

No preview available before publication


Keywords <mark>Ecological restoration</mark> <mark>eDNA</mark> <mark>Bioinformatics</mark> <mark>Ultramafic soil</mark> <mark>Metabarcoding</mark> <mark>eDNA</mark> <mark>ITS2</mark> <mark>V4</mark> <mark>Ultramafic soils</mark>

### References

- Fernandez Nuñez N., Maggia L., Stenger PL., Lelievre M., Letellier K., Gigante S., Manez A., Mournet P., **Ripoll J.**, Carriconde F. <mark>2021</mark>. Potential of high-throughput eDNA sequencing of soil fungi and bacteria for monitoring ecological restoration in ultramafic substrates: The case study of the New Caledonian biodiversity hotspot. Ecological Engineering, 173, 106416, ISSN 0925-8574 <a href="https://doi.org/10.1016/j.ecoleng.2021.106416" target="_blank">DOI</a>

- Carriconde F., Maggia L., Stokes A., Gardes M., Dinh K., Fernandez N., Demenois J., Stenger P.L., Ripoll J., Read J., Mournet P. <mark>2023</mark>. Nancy : ISES, 1 p.. International Conference on Serpentine Ecology (ICSE 2023). **Nancy, France** (June 12-16). <a href="https://publications.cirad.fr/une_notice.php?dk=605101" target="_blank">DOI</a>

- Ripoll J. <mark>2017</mark>. Complexité de l'analyse bioinformatique des données méta-génomiques/transcriptomiques. Séminaire «Les nouvelles techniques de génomiques: applications, contraintes et perspectives ». **University of New Caledonia, Noumea** (December 12).