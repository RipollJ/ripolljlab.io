---
layout: default
title: ScOre FTO
subtitle: Post-transcriptional control and cell adaptation.
---

### FTO-mediated cytoplasmic m<sup>6</sup>A<sub>m</sub> demethylation adjusts stem-like properties in colorectal cancer cell

- Research associates 2020-2021
- Institutes:
  - LIRMM, Montpellier, France

<div class="logos">
    <ul><li><img src="/assets/img/logo/lirmm.png" alt="logo LIRMM" /></li></ul>
</div>

<div class="mosaic">
    <img src="/assets/img/fto/fto_treat.png" />
    <img src="/assets/img/fto/FTO.png" />
    <img src="/assets/img/fto/fto_m6a.png" class="wide"/>
</div>


**Team members & co-workers**

- Relier Sébastien (IGF)
- David Alexandre (IGF)
- Bastide Amandine (IGF)
- Choquet Armelle (UM)
- Rivals Eric (CNRS)

### Abstract

Cancer stem cells (CSCs) are a small but critical cell population for cancer biology since they display inherent resistance to standard therapies and give rise to metastases. Despite accruing evidence establishing a link between deregulation of epitranscriptome-related players and tumorigenic process, the role of messenger RNA (mRNA) modifications in the regulation of CSC properties remains poorly understood. 

Here, we show that the cytoplasmic pool of fat mass and obesity-associated protein (FTO) impedes CSC abilities in colorectal cancer through its N6,2’-O-dimethyladenosine (m6Am) demethylase activity. While m6Am is strategically located next to the m7G-mRNA cap, its biological function is not well understood and has not been addressed in cancer. Low FTO expression in patient-derived cell lines elevates m6Am level in mRNA which results in enhanced in vivo tumorigenicity and chemoresistance.

Inhibition of the nuclear m6Am methyltransferase, PCIF1/CAPAM, fully reverses this phenotype, stressing the role of m6Am modification in stem-like properties acquisition. FTO-mediated regulation of m6Am marking constitutes a reversible pathway controlling CSC abilities. Altogether, our findings bring to light the first biological function of the m6Am modification and its potential adverse consequences for colorectal cancer management.

Keywords <mark>Bioinformatics</mark> <mark>Translatome</mark> <mark>Transcriptome</mark> <mark>Splicing</mark> <mark>m6A methylation</mark> <mark>Oncology</mark>

### References

- Relier S., **Ripoll J.**, Guillorit H., Amalric A., Boissière F., Vialaret J., Attina A., Debart F., Choquet A., Macari F., Samalin E., Vasseur JJ., Pannequin J., Crapez E., Hirtz C., Rivals E., Bastide A., David A. <mark>2021</mark>. FTO-mediated cytoplasmic m<sup>6</sup>A<sub>m</sub> demethylation adjusts stem-like properties in colorectal cancer cell. Nature Communications, 12, 1716. <a href="https://doi.org/10.1038/s41467-021-21758-4" target="_blank">DOI</a> Previous version available on HAL in 2020 <a href="https://hal.archives-ouvertes.fr/hal-03000860/" target="_blank">DOI</a>

- Ripoll J. <mark>2019</mark>. Understanding translational regulation processes in cancer cells. Doctorate and post-doctorate MAB days. LIRMM, **Montpellier, France** (24 Sept. 2019).

- Ripoll J., Relier S., Bastide A., David A., Rivals E. <mark>2020</mark>. Comparative transcriptomics upon shutdown of a major player in human epitranscriptome regulation. JOBIM 2020. **Montpellier, France** (30th June - 2th July). Digital poster
