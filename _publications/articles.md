- Chalabi-Dchar M., Villeronce O., **Ripoll J.**, Vincent A., Fenouil T., Khoueiry R., Kucharczak J., Jentschel L., Catez F., Vigneron A., Tréguier J., Mandierc., Bouclier C., Vitre J., Lagerqvist L., Choquet A., Herceg Z., Machon C., Guitton J., David A., Solary E., Bernard D., Martin N., Rivals E., Dalla Venezia N., Pannequin J., Diaz JJ. **2024**. Translational control of cell plasticity drives 5-FU tolerance. *BioArXiv* <a href="https://doi.org/10.1101/2024.07.03.601826 " target="_blank">DOI</a>

- Chevalier C., Dorignac J., Ibrahim Y., Choquet A., David A., **Ripoll J.**, Rivals E., Geniet F., Walliser N.-O., Palmeri J., Parmeggiani A., Walter J.-C. **2023** Physical modeling of ribosomes along messenger RNA: Estimating kinetic parameters from ribosome profiling experiments using a ballistic model. *PLoS Comput Biol* 19(10): e1011522. <a href="https://doi.org/10.1371/journal.pcbi.1011522" target="_blank">DOI</a>

- Baron T., **Ripoll J.**, Dresch C., Charles F., Vidal V., Djari A., Maza E., Laurent S., Chervin C., Sallanon H. **2023** Transcriptomic response of lettuce ( Lactuca sativa L.) leaves to a postharvest intermittent-light treatment during storage. *Acta Horticulturae 1364. XXXI International Horticultural Congress* (IHC2022): International Symposium on Postharvest Technologies to Reduce Food Losses. <a href="http://dx.doi.org/10.17660/ActaHortic.2023.1364.18" target="_blank">DOI</a>

- Mille M., **Ripoll J.**, Cazaux B., Rivals E. **2023** Dipwmsearch: a python package for searching di-PWM motifs. *Bioinformatics* 39(4). <a href="http://dx.doi.org/10.1093/bioinformatics/btad141" target="_blank">DOI</a>

- Chevalier C., et al. **ArXiv 2022**. Physical modeling of ribosomes along messenger RNA: estimating kinetic parameters from ribosome profiling experiments using a ballistic model. <a href="https://doi.org/10.48550/arXiv.2208.12576" target="_blank">DOI</a>

- Therizols G., Bash-Imam Z., Panthu B., Machon C., Vincent A., **Ripoll J.**, et al. **2022**. Alteration of ribosome function upon 5-fluorouracil treatment favors cancer cell drug-tolerance. *Nature Communications* 13, 173. <a href="https://doi.org/10.1038/s41467-021-27847-8" target="_blank">DOI</a>

- Relier S., **Ripoll J.**, Guillorit H., Amalric A., Boissière F., Vialaret J., Attina A., Debart F., Choquet A., Macari F., Samalin E., Vasseur JJ., Pannequin J., Crapez E., Hirtz C., Rivals E., Bastide A., David A. **2021**. FTO-mediated cytoplasmic m<sup>6</sup>A<sub>m</sub> demethylation adjusts stem-like properties in colorectal cancer cell. *Nature Communications*, 12, 1716. <a href="https://doi.org/10.1038/s41467-021-21758-4" target="_blank">DOI</a> Previous version available on HAL in 2020 <a href="https://hal.archives-ouvertes.fr/hal-03000860/" target="_blank">DOI</a>

- Fernandez Nuñez N., Maggia L., Stenger PL., Lelievre M., Letellier K., Gigante S., Manez A., Mournet P., **Ripoll J.**, Carriconde F. **2021**. Potential of high-throughput eDNA sequencing of soil fungi and bacteria for monitoring ecological restoration in ultramafic substrates: The case study of the New Caledonian biodiversity hotspot. *Ecological Engineering*, 173, 106416, ISSN 0925-8574 <a href="https://doi.org/10.1016/j.ecoleng.2021.106416" target="_blank">DOI</a>

- **Ripoll J.**, Charles F., Vidal V., Laurent S., Klopp C., Laurie F., Sallanon H., Roux D. **2019**. Transcriptomic view of detached lettuce leaves during storage: A crosstalk between wounding, dehydration and senescence. *Postharvest Biology and Technology*, 152: 73-88. <a href="https://doi.org/10.1016/j.postharvbio.2019.02.004" target="_blank">DOI</a>

- Lecompte F., Nicot P.C., **Ripoll J.**, Abro M.A., Rimbault A.K., Lauri F., Bertin, N. **2017**. Reduced susceptibility of tomato stem to the necrotrophic fungus _Botrytis cinerea_ is associated with a specific adjustment of fructose content in the host sugar pool. *Annals of Botany*, 119(01):1-13- DOI: 10.1093/aob/mcw240. <a href="https://doi.org/10.1093/aob/mcw240" target="_blank">DOI</a>

- **Ripoll J.**, Bertin N., Bidel L.P.R., Urban L. **2016**. A User's View of the Parameters Derived from the Induction Curves of Maximal Chlorophyll a Fluorescence: Perspectives for Analyzing Stress. *Frontiers in Plant Science*. <a href="https://doi.org/10.3389/fpls.2016.01679" target="_blank">DOI</a>

- **Ripoll J.**, Urban L., Bertin N. **2016**. The potential of the MAGIC TOM parental accessions to explore the genetic variability in tomato acclimation to repeated cycles of water deficit and recovery. *Frontiers in Plant Sciences*. 6:1172 <a href="https://doi.org/10.3389/fpls.2015.01172" target="_blank">DOI</a>

- **Ripoll J.**, Urban L., Brunel B., Bertin N. **2016**. Water deficit effects on tomato quality depend on fruit developmental stage and genotype. *Journal of Plant Physiology*. 190: 26-35. <a href="https://doi.org/10.1016/j.jplph.2015.10.006" target="_blank">DOI</a>

- **Ripoll J.** **2015**. Effects of water stress, only or in interaction with a pathogen, on plant functioning and fruit quality, depending on genetic variation. PhD Thesis. <a href="https://hal.archives-ouvertes.fr/tel-01512258" target="_blank">DOI</a>

- **Ripoll J.**, Urban L., Brunel B., L'Hôtel J-C., Garcia G., Bertin N. **2015**. Impact of water deficit on tomato fruit growth and quality depends on the fruit developmental stage. *Acta Horticulturae*. <a href="https://doi.org/10.17660/ActaHortic.2016.1112.24" target="_blank">DOI</a>

- Urban L., Staudt M., **Ripoll J.**, Lauri F., Bertin N. **2015**. Less can make more - revisiting fleshy fruit quality and irrigation in horticulture. Chronica Horticulturae. 54 (4), p. 24-31. <a href="https://hal.inrae.fr/hal-01331456" target="_blank">DOI</a>

- **Ripoll J.**, Urban L., Staudt M., Lopez-Lauri F., Bidel L.P.R., Bertin N. **2014**. Water shortage and quality of fleshy fruits - making the most of the unavoidable. *Journal of Experimental Botany*, 65(15), p. 4097-4117. <a href="https://doi.org/10.1093/jxb/eru197" target="_blank">DOI</a>

- **Ripoll J.**, Bon M.-C., Jones W.A. **2011**. Optimization of the genomic DNA extraction method of silverleaf nightshade (_Solanum elaeagnifolium_ Cav.), an invasive plant in the cultivated areas within the Mediterranean region. *Biotechnologie, Agronomie, Société et Environnement*, 15(1), p.95-100. <a href="https://popups.uliege.be/1780-4507/index.php?id=7020" target="_blank">DOI</a>
