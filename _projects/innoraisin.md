---
layout: default
title: CASDAR INNOHORT
subtitle: Water deficit & Vitis vinifera (Patent)
---

### INNORAISIN - Nutritional quality of grapes : a new lever that can be mobilized by the industry to meet consumer’s expectations and preferences

- Post-doctorate 2015-2016
- Institutes:
  - University UAPV, Avignon, France
  - CTIFL, St Rémy-de-Provence, France
  - Agronutrition, Carbonne, France
  - Domaine La Tapy, Carpentras, France

<div class="logos">
    <ul>
        <li><img src="/assets/img/logo/uapv.png" alt="logo UAPV" /></li>
        <li><img src="/assets/img/logo/ctifl.png" alt="logo CTIFL" /></li>
        <li><img src="/assets/img/logo/agronutrition.png" alt="logo Agronutrition" /></li>
        <li><img src="/assets/img/logo/latapy.png" alt="logo Tapy" /></li>
   </ul>
</div>


<div class="mosaic">
    <img src="/assets/img/innoraisin/fluorescence.png" class="big"/>
    <img src="/assets/img/innoraisin/grappe.jpg" />
    <img src="/assets/img/innoraisin/correlation_plant_fruit.png" />
</div>


**Team members & co-workers**

- Urban Laurent (UAPV)
- Aarrouf Jawad (UAPV)
- Aubert Christophe (CTIFL)

### Abstract

Due to patent confidentiality, no informations are available here.
Sorry for the inconvenience.

Missions

1/ Processing of agronomic and physiological data acquired during the 2014 and 2015 measurement campaigns.  The aim is to validate the hypothesis that physiological stress markers were impacted by water deficits and plant defense stimulants (PDS) applied to grapevines during the 2014 and 2015 campaigns. The hypothesis of an impact of PDS on photosynthetic water efficiency will also be tested.

2/ Correlate these data with yield and berry quality data obtained by our partners, notably from the La Tapy experimental station and Ctifl. The main objective is to test the hypothesis that water stress and PDS stimulate the synthesis of secondary compounds. Positive or negative correlations with yield will be investigated.

3/ Write scientific articles presenting the results obtained.

Keywords <mark>Vitis vinifera</mark> <mark>Water deficit</mark> <mark>Plant ellictor</mark> <mark>Ecophysiology</mark> <mark>Fruit quality</mark> <mark>Post-harvest conservation</mark> <mark>Organic farming</mark> <mark>Plant defense</mark> <mark>Data Analysis</mark>

### References

Information available in press: <a href="https://www.arboriculture-fruitiere.com/articles/technique-fruit/un-casdar-pour-le-raisin-de-table" target="_blank">Article</a>