---
layout: default
title: COST Translacore
subtitle: CA21154 - Translational control in Cancer European Network (TRANSLACORE)
---

### Workflow for translatome analysis

- Research associates 2019-2021 / Project Leader 2022-2023
- Institutes:
  - LIRMM, Montpellier, France

<div class="logos">
    <ul><li><img src="/assets/img/logo/lirmm.png" alt="logo LIRMM" /></li></ul>
</div>


<div class="mosaic c2">
    <img src="/assets/img/translatome/Pipelines.png" />
    <img src="/assets/img/translatome/log_plot.png" />
</div>


**Team members & co-workers**

- Fati Chen (LIRMM-UM)
- Céline Mandier (LIRMM-CNRS)
- Rivals Eric (LIRMM-CNRS)

### Abstract

RNA sequencing (RNA-seq) is often used to elucidate the regulation of gene expression, as it provides an in-depth view of transcribed RNAs and a relative quantification of each gene or transcript. However, several studies have found that variations in RNA transcript levels do not necessarily correlate well with protein levels, suggesting that translation also plays a key role. To study the role of translation in regulating gene expression, the translatome can be studied by selecting ribosome-bound RNAs, releasing ribosomes, and then sequencing the selected population of RNAs as in RNA-seq. This approach, called polysome sequencing (POL-seq), is a census assay that provides relative expression levels of genes/transcripts during translation. Using the workflow manager Snakemake and Conda environments, we developed a bioinformatics pipeline to jointly analyze these transcriptome and translatome fractions. 

To promote reusability of individual steps, we propose a lightweight wrapper system to facilitate interoperability on different hardware without requiring an Internet connection. Moreover, it preserves the command inspection in Snakemake's dry-run mode.

This pipeline is divided into two parts for primary and secondary analysis. The primary analysis part consists of 4 steps: i) quality control, ii) cleaning of the sequencing reads, iii) mapping of the reads to the reference genome, and iv) counting of the mapped reads per gene. The secondary analysis part encapsulates all statistical analyses to estimate the differential expression of genes and then perform functional enrichment analyses (i.e. gene ontology and pathway databases).

For the statistical part, all samples are normalized together to avoid any comparison problems between fractions. We then combine the comparisons of each fraction, visualized by a scatter plot, and filter the data according to their transcriptional and translational expression levels. This allows us to classify differentially expressed mRNAs into eight categories. These categories represent mRNAs that are regulated by transcription only, by translation only, or by both transcription and translation. In the case of conjoint regulations, we determine whether these regulations have combined or opposing effects.

We will illustrate this pipeline with results from a publication with our collaborators (Therizols et al., 2022).

Keywords <mark>Bioinformatics</mark> <mark>Translatome</mark> <mark>Transcriptome</mark> <mark>Workflow</mark> <mark>Snakemake</mark> <mark>Conda</mark>

### References

- Ripoll J. <mark>2022</mark>. Joint Transcriptome and Translatome Analysis: A Reproducible Pipeline and an Example in Cancer Research. Genotoul Biostat Bioinfo days. LIRMM, **INRAE, Auzeville, France** (30 Nov. 2022). <a href="https://bioinfo-biostat.sciencesconf.org/data/pages/genotoul_ripoll_2022.pdf" target="_blank">DOI</a>

- Ripoll J. <mark>2022</mark>. Snakemake & Workflow Hub (2022). Séminaire Bioinformatique MBI. **Genopolys, Montpellier, France** (24 Mar. 2022). <a href="https://gitlab.in2p3.fr/mbi/mbi/-/blob/master/20220106_RegistresPipelines/Workflow_Hub_mbi_ripoll.pdf" target="_blank">DOI</a>

- Ripoll J., Mandier C., Chen F., Rivals E.  <mark>2023</mark>. Joint transcriptome and translatome analysis: a reproducible pipeline. JOBIM 2023, **Nice, France** (June 27-30). <a href="https://jobim2023.sciencesconf.org/data/pages/proceedings.pdf" target="_blank">DOI</a>
