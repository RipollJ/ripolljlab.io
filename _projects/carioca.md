---
layout: default
title: ANR CARIOCA
subtitle: Acclimatization of corals to ocean acidification around underwater CO2 resurgences - CARiOCA
---

### Acclimatation des coraux à l'acidification des océans : étude par transcriptomique comparative, après transferts réciproques, de coraux vivant ou non à proximité de résurgences sous-marines en CO2

- Post-doctorate 2017-2018
- Institutes:
  - IRD, Noumea, New Caledonia

<div class="logos">
    <ul>
        <li><img src="/assets/img/logo/ird.png" alt="logo IRD" /></li>
    </ul>
</div>


<div class="mosaic ">
    <img src="/assets/img/carioca/vents_2_small.jpg" class="full"/>
    <img src="/assets/img/carioca/gaz_exchange_small.jpg" class="tall" />
    <img src="/assets/img/carioca/Capture_table_small.jpg" class="wide" />
    <img src="/assets/img/carioca/Co2_seeps_small.jpg" class="tall"/>
    <img src="/assets/img/carioca/Upa-Upasina.PNG" />
    <img src="/assets/img/carioca/growth_small.jpg" />
    <img src="/assets/img/carioca/vents_small.jpg" />
</div>


**Team members & co-workers**

- Berteaux-Lecellier Véronique (CNRS)
- Lecellier Gaël (University, Versailles)
- Rodolfo-Metalpa Riccardo (IRD)
- Biscéré Tom (IRD)
- Magalon Hélène (IRD)
- Gérin Pauline (IRD)
- Houlbreque Fanny (IRD)

### Abstract

Une grande partie de notre compréhension des effets de l'acidification et du réchauffement des océans sur les écosystèmes marins tropicaux est issue des résultats d'expériences en laboratoire réalisées à court terme, ou de modèles déterministes (par exemple Kaniewska et al. 2012; Daniel et al. 2013.). Bien que de telles expériences de laboratoire soient indispensables, elles sont souvent trop brèves pour que l'organisme ait réellement eu le temps de s'acclimater aux conditions expérimentales, et les facteurs co-limitants (tels que les nutriments, les courants) sont difficiles à simuler ex situ. À ce jour, le seul outil pour étudier la capacité des coraux à s'adapter aux niveaux d'acidification projetés pour la fin du siècle est l'utilisation de résurgences sous-marines, où le CO2 s'échappant continuellement des fonds sous-marins modifie la chimie de l'eau de mer. Il est à noter que ces résurgences ont été continuellement actives depuis des siècles, voire des millénaires. Ces zones, avec de hauts niveaux en pCO2 où le pH et la saturation en carbonates sont faibles, offrent des systèmes adéquats pour l'étude des effets à long terme de l'acidification des océans sur les organismes et les populations de coraux.

Ainsi, depuis 2008 (Hall-Spencer et al. 2008; Rodolfo-Metalpa et al. 2011), plusieurs études ont utilisé ces résurgences sous-marines naturelles comme laboratoires naturels pour étudier les réponses des récifs coralliens à l'acidification des océans. Fabricius et al. (2011) ont exploré le système à proximité des îles Entrecasteaux de la province de Milne Bay, en Papouasie-Nouvelle-Guinée, montrant que certains coraux étaient capables de contrecarrer les pH acides. Globalement, les études réalisées sur les résurgences des îles d'Entrecasteaux en Papouasie-Nouvelle-Guinée ont révélé, en condition de faibles pH chroniques, des récifs dominés par des communautés de coraux massifs, tandis que les espèces à croissance rapide en sont exclues. Ainsi, certains récifs avec une couverture corallienne élevée peuvent exister dans une eau de mer de pH 7,8, toutefois ils présentent une communauté corallienne modifiée.

Ces études in situ montrent donc qu'il est possible pour certaines espèces de coraux (dénommées ci-après espèces gagnantes) de survivre dans des conditions environnementales proches de celles projetées pour la fin du siècle. Au contraire, certaines espèces en sont incapables (espèces perdantes). Ces observations soulèvent donc une question cruciale : quelles espèces coralliennes participeront encore à la configuration récifale dans les 50-100 prochaines années, lorsque l'océan sera devenu plus acide? En d'autres termes, peut-on prédire le devenir (mort/survie) d'une espèce ? Afin d'avancer dans les projections, il est maintenant indispensable d'aller plus loin dans la compréhension des mécanismes biologiques impliqués dans la survie du corail dans un environnement acide.

Ce projet original vise, par une approche intégrative alliant analyses physiologiques, génétiques et « omiques », et en tirant profit de l'existence de ces zones de résurgences naturelles en Papouasie-Nouvelle Guinée, à une meilleure compréhension des processus d'acclimatation/adaptation des coraux à un environnement acide. A l'aide d'expériences de transferts réciproques et croisés de coraux gagnants et perdants, dans des zones acides ou témoin.

Keywords <mark>Corals</mark> <mark>Ecophysiology</mark> <mark>Bioinformatics</mark> <mark>Metabolisms</mark> <mark>Abiotic stress</mark> <mark>Metatranscriptome</mark> <mark>Ocean acidification</mark> <mark>Genetic diversity</mark> <mark>De novo analyses</mark>


### References

- Ripoll J. <mark>2018</mark>. Acclimatization of three coral species to ocean acidification around natural CO2 seeps: transplantation & gene expression. 4th APCRS congress. **Cebu, Philippines** (June 3-9).
