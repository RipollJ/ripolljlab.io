---
layout: default
title: EBCL S. elaeagnifolium
subtitle: Biological Pest Control
---

### Contribution of genetic and life history traits to the invasive power of Sylverleaf nightshade as part of a biological control program

- Master 2 trainee - 2010
- Institute:
  - EBCL, Montpellier, France

<div class="logos">
    <ul><li><img src="/assets/img/logo/ebcl.png" alt="EBCL logo" /></li></ul>
</div>


<div class="mosaic">
    <img src="/assets/img/solanum-elaeagnifolium/Solanum_elaeagnifolium.jpg" />
    <img src="/assets/img/solanum-elaeagnifolium/caryotype.jpg" />
    <img src="/assets/img/solanum-elaeagnifolium/ebcl_ladder.png" class="full" />
</div>


**Team members & co-workers**

- Bon Marie-Claude (EBCL)
- Jones Walker (EBCL)

### Abstract

The silverleaf nightshade, Solanum elaeagnifolium Cav. (Solanaceae) is a perennial and suckering weed that originates from the Subtropical America from where it has been introduced but not intentionally in several geographical zones over the world including the Mediterranean Bassin. In Greece, this invasive is considered as a serious problem for crops and natural areas. The aim of this work was to trace the origin of the invasion of the silverleaf nightshade in the continental and insular Greece using molecular markers of two types, i.e. chloroplastic and ISSR and to assess if the species invasiveness could be explained by different levels of genetic diversity and of life history traits linked with vegetative development and growth. The results obtained based on these two genetic markers gave evidence of high levels of genetic diversity in the native range but also in continental Greece. They suggested that the species was introduced from Texas more than one time and most likely from a high number of founders. Among all the life historical traits considered, the fresh weight of roots was significantly different between the two origins. Such genetic information would be integrated in the work plan of the biological control program against the silverleaf nightshade in Greece.


Keywords <mark>Solanum elaeagnifolium Cav.</mark> <mark>Biological Invasion</mark> <mark>USDA</mark> <mark>ISSR</mark> <mark>Intergenic chloroplastic region trnS-trnG</mark> <mark>Life history traits</mark> <mark>DNA extraction</mark> <mark>multiplex PCR</mark> <mark>Sequencing</mark> <mark>Cytogenetic</mark> <mark>Quarantine (EPPO class 2)</mark> <mark>Phenotyping</mark>

### References

- **Ripoll J.**, Bon M.-C., Jones W.A. <mark>2011</mark>. Optimization of the genomic DNA extraction method of silverleaf nightshade (_Solanum elaeagnifolium_ Cav.), an invasive plant in the cultivated areas within the Mediterranean region. Biotechnologie, Agronomie, Société et Environnement, 15(1), p.95-100. <a href="https://popups.uliege.be/1780-4507/index.php?id=7020" target="_blank">DOI</a>

- Ripoll J. <mark>2010</mark>. Contribution de la diversité génétique et des traits d'histoire de vie au pouvoir envahissant de la Morelle Jaune Solanum elaegnifolium Cavanilles (Solanacae) dans un contexte de programme de lutte biologique. Séminaire Printemps de Baillarguet 2010, **Montferrier sur lez, France** (April 29).

- Ripoll J., Guermache F., Jeanneau M., Kashefi J., Coleman R., Briano J., Jones W.A., Strickman D., Bon M-C. <mark>2010</mark>. Contribution de la diversité génétique et de traits d'histoire de vie au pouvoir envahissant de la Morelle Jaune, Solanum elaeagnifolium Cav, dans un contexte de programme de lutte biologique. Colloque national Ecologie 2010, **Montpellier** (September 2-4).