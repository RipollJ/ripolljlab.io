---
layout: default
title: LabEx NUMEV GEM
subtitle: Information fuelled biophysical models for the control of gene expression
---

### Gene Expression Modeling (GEM)

- Research associates 2019-2021
- Institutes:
  - LIRMM, Montpellier, France

<div class="logos">
    <ul><li><img src="/assets/img/logo/lirmm.png" alt="logo LIRMM" /></li></ul>
</div>


<div class="mosaic">
    <img class="wide" src="/assets/img/gem/carole_model.png" class="big" />
    <img class="wide" src="/assets/img/gem/armelle_polysome.png" />
</div>


**Team members & co-workers**

- Rivals Eric (LIRMM-CNRS)
- David Alexandre (IGF-CNRS)
- Relier Sébastien (IGF-CNRS)
- Bastide Amandine (IGF-CNRS)
- Choquet Armelle (UM)
- Attina Aurore (CHU Plateforme de protéomique clinique)
- Palmeri John (UM)
- Geniet Frédéric (UM)
- Walter Jean-Charles (UM)
- Dorignac Jérôme (UM)
- Chevalier Carole (UM)
- Parmeggiani Andrea (UM)
- Walliser Nils-Ole (UM)
- Soudon Paul (UM)

### Project

For long, translation of messenger RNAs into protein was thought to be a uniform, constant mechanism that had almost no impact on gene expression level. However, we know now that the translation machinery acts as a regulator of gene expression by preferentially selecting some mRNAs for translation, by translating more efficiently some mRNAs more than others, etc. To estimate this effect on the expression level of a gene, one computes the translational efficiency by comparing the Ribo-seq versus the RNA-seq profile of that mRNA.
Analysing ribosome profiling data can also indicates how ribosomes process an mRNA during translation, where they start, pause, accelerate or stop. This raises the question of modelling translation using various approaches including statistical physics. By analysing Ribosome profiling data from various experiments, we wish to discover
the whole set of translated RNAs, including small ORFs, and to unravel the mechanisms of translational regulation.

### Part I: Physical modeling of ribosomes along messenger RNA: estimating kinetic parameters from ribosome profiling experiments using a ballistic model

Gene expression consists in the synthesis of proteins from the information encoded on DNA. One of the two main steps of gene expression is the translation of messenger RNA (mRNA) into polypeptide sequences of amino acids. Here, by taking into account mRNA degradation, we model the motion of ribosomes along mRNA with a ballistic model where particles advance along a filament without excluded volume interactions. Unidirectional models of transport have previously been used to fit the average density of ribosomes obtained by the experimental ribo-sequencing (Ribo-seq) technique. In this case an inverse fit gives access to the kinetic rates: the position-dependent speeds and the entry rate of ribosomes onto mRNA. The degradation rate is not, however, accounted for and experimental data from different experiments are needed to have enough parameters for the fit. Here, we propose an entirely novel experimental setup and theoretical framework consisting in splitting the mRNAs into categories depending on the number of ribosomes from one to four. We solve analytically the ballistic model for a fixed number of ribosomes per mRNA, study the different regimes of degradation, and propose a criteria for the quality of the inverse fit. The proposed method provides a high sensitivity to the mRNA degradation rate. The additional equations coming from using the monosome (single ribosome) and polysome (arbitrary number) ribo-seq profiles enable us to determine all the kinetic rates in terms of the experimentally accessible mRNA degradation rate.

Keywords <mark>Bioinformatics</mark> <mark>Biophysics</mark> <mark>Translation</mark>

### References

- Chevalier C., et al. <mark>ArXiv</mark>. Physical modeling of ribosomes along messenger RNA: estimating kinetic parameters from ribosome profiling experiments using a ballistic model. <a href="https://doi.org/10.48550/arXiv.2208.12576" target="_blank">DOI</a>

- Chevalier C. <mark>Theses.fr</mark>. Modèles physiques pour l'étude de la cinétique de transport des ribosomes sur l'ARN messager lors de la traduction génétique et l'interprétation des données de ribosome profiling. <a href="https://www.theses.fr/2022UMONS075" target="_blank">DOI</a>