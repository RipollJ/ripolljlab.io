---
layout: default
title: CIRAD Elaeidobius
subtitle: 1st Internship
---

### Interactions between the inflorescences of the oil palm Elaeis guineensis J. and their pollinators of the genus Elaeidobius spp. F. (Coleoptera: Curculionidae) in Central Africa

- Master 1 trainee - 2009
- Institutes:
  - CIRAD, Montpellier, France
  - SOCAPALM, Kribi, Cameroun
  - INRA, Versaille, France

<div class="logos">
    <ul>
        <li><img src="/assets/img/logo/cirad.png" alt="logo CIRAD" /></li>
        <li><img src="/assets/img/logo/socapalm.png" alt="logo SOCAPALM" /></li>
        <li><img src="/assets/img/logo/inrae.png" alt="logo INRAE" /></li>
   </ul>
</div>


<div class="mosaic">
    <img class="tall" src="/assets/img/elaeidobius/pieges_fleur_femelle_small.jpg">
    <img src="/assets/img/elaeidobius/Elaeidobius.jpg" >
    <img src="/assets/img/elaeidobius/fleur_male_small.jpg">
    <img src="/assets/img/elaeidobius/pompe_small.jpg">
</div>


**Team members & co-workers**

- Beaudoin-Ollivier Laurence (CIRAD)
- Frérot Brigitte (INRA)
- Morin Didier (CIRAD)
- Meyobeme Huguette (Socapalm)
- Flori Alber (CIRAD)

### Abstract

Four species of the genus Elaeidobius present in Cameroon play an important role in the pollination of the oil palm, Elaeis guineensis. We study the attendance of the two inflorescences by these insects according to the stadium of anthesis and climatic conditions during 6 weeks on a plantation in Cameroon. We also work on the volatile organic compounds emitted by the inflorescences of E. guineensis and their attraction on these insects. Our results show that the Elaeidobius subvittatus are more present in the field by sunny weather. The rain has a negative effect on the activity of Elaeidobius spp. flight which are less numerous on interception’s traps set up close to the males inflorescences and are absent near the females inflorescences. The distribution of the four studied species is different according to the anthesis’ stadium of the inflorescences. The tests of attraction of the volatile organic compounds to these insects reveal a stronger attraction of the males and females inflorescences into anthesis which emit both the same main compound, estragole. It also emerges that the male inflorescences into anthesis are more attractive than female inflorescences for the four species.

Keywords <mark>Elaeis guineensis, J.</mark> <mark>Pollination</mark> <mark>Elaeidobius spp.</mark> <mark>Volatil Organic compounds</mark> <mark>Cameroon</mark> <mark>Interactions</mark> <mark>Palm</mark> <mark>Chemical Ecology</mark> 

### References

- Beaudoin-Ollivier L., Frérot B., Ripoll J., et al. <mark>2010</mark>. Comparative activity of four Elaeidobius spp oil palm pollinators visiting oil palm inflorescences in Central Africa. Congress Palms 2010, International Symposium on the biology of the Palm family, **Montpellier** (May 5-7). <a href="https://agritrop.cirad.fr/578124/" target="_blank">DOI</a>

- Frérot B., Beaudoin-Ollivier L., Ripoll J., Meyobeme H. <mark>2010</mark>. Mutualisme et pollinisation par duperie olfactive : Cas du palmier à huile et des Elaeidobius. Oral communication, 16ème colloque sur la biologie de l'insecte, **Lyon, France** (October 18-20). 

- Frérot B., Beaudoin-Ollivier L., Ripoll J., Meyobeme H. <mark>2010</mark>. Deception pollination in oil palm tree. Oral communication, PALMS 2010 International Symposium, **Montpellier, France** (May 5-7)