---
layout: default
title: Publications
subtitle: All communications and productions
---

### Software

{% include_relative _publications/softwares.md %}

### Coming soon

- **Ripoll J.**, et al. <mark>In prep.</mark>. Microbial Diversity in New Caledonia's Ultramafic Ecosystems: Insights for Conservation and Sustainable Land Management in a Biodiversity Hotspot. *Provisional title*

- **Ripoll J.**, et al. <mark>In prep.</mark>. Acclimatization of three coral species to ocean acidification around natural CO<sub>2</sub> seeps: transplantation & gene expression. *Provisional title*

- **Ripoll J.**, et al. <mark>In prep.</mark>. FTO-deficient cells involve cell type-dependent regulation of mRNA demethylations and alternative splicing. *Provisional title*

- **Ripoll J.**, et al. <mark>In prep.</mark>. Translatome Regulations Analysis In a Nutshell (TRAIN): a Snakemake workflow for joint transcriptome and translatome .categorization. *Provisional title*

- Mourksi N., Isaac C., Morin C., **Ripoll J.**, et al. <mark>In prep.</mark>. Translational reprogramming in crizotinib resistant lung adenocarcinoma. *Provisional title*

### Articles

{% include_relative _publications/articles.md %}

### Oral communications

{% include_relative _publications/communications.md %}

### Posters

{% include_relative _publications/posters.md %}

### Associated communications

{% include_relative _publications/associates.md %}
