---
layout: default
title: Experiences
subtitle: PhD Ecophysiologist & Expert in Data Sciences - Bioinformatics
---

<div class="grid">
  <section>
    <h3>Education</h3>
    <h5>Qualifications for teachings in University</h5>
    <ul>
      <li>Qualifications CNU (2022-2026): sections 64 (bioinformatics), and 68 (organisms biology)</li>
      <li>Qualifications CNU (2017-2022): sections 66 (physiology), 67 (ecology) and 68 (organisms biology)</li>
    </ul>
    <h5>Certifications</h5>
    <ul>
      <li>Kubernetes (2025, edX)</li>
      <li>Numerical computing with RUST (2024, CCFR)</li>
      <li>FIDLE deep learning (2022, CNRS)</li>
      <li>Atos Quantum Learning Machine (2019, CINES)</li>
      <li>MOOC 41001S02 'Python' (2015, INRIA)</li>
    </ul>
    <h5>Thesis in Agronomic Sciences</h5>
    <ul>
      <li>University of Avignon</li>
      <li>Validated in 2015 with mention TH</li>
      <li>
        Keywords: Physiology, Fruit quality, Water deficit, B. cinerea, OJIP
        model, Genetic variability
      </li>
    </ul>
    <h5>Master Bio-engineering</h5>
    <ul>
      <li>University of Montpellier</li>
      <li>Validated in 2010 with mention B</li>
      <li>
        Keywords: Biodetection kit, DNA chips, RFID, Project managing, ISO 9001
        and 14001, HACCP, Sequencing, Bioinformatics
      </li>
    </ul>
    <h5>Bachelor degrees Biology-Ecology</h5>
    <ul>
      <li>University of Perpignan</li>
      <li>Validated in 2008 with mention</li>
      <li>
        Keywords: Cellular and bacterial cultures, API gallery, Antibiogram,
        SIG, Fire risk training, Ecology, Parasitology
      </li>
    </ul>
  </section>
</div>

---
### Experiences

**Volunteer - professional break** From oct. 2023 to today

Activities:
- Writing scientific articles, Workflows maintenance, Jury, Reviewer
- Teachings: Introduction to statistics and R for PhD student ; Acadomia (Computer Science)
- Co-organization of **The 1st Young Scientist Cancer Congress** (YS2C) : "New research strategies for cancer therapy – from bench to bedside". October 5, 2023 at Toulouse, France. More informations: <a href="https://www.crct-inserm.fr/en/ys2c_gso/1" target="_blank">https://www.crct-inserm.fr/en/ys2c_gso/</a>

---
**Researcher Bioinformatics & Data Science** From Jan. 2019 to sept. 2023

- Company: LIRMM-UM-CNRS
- Location: Montpellier, France

I worked on different subjects related to cancer research (specific gene impact, chemotherapy), to fundamental research (mRNA modelisation). I developed methods and design workflows for diverses NGS sequencing, realize statistical analysis and perform new visualisations.

Projects:
- [GEM](/projects/gem)
- [dipwmsearch](/projects/dipwm)
- [FTO](/projects/fto)
- [5-FU](/projects/5fu)
- [translatome](/projects/translatome)
- RESIST2OME
- TRANSLATOL
- MELANOMA
- Splicing

---
**Post-doc Bioinformatics & Data Science** From nov. 2018 to dec. 2018

- Company: Institut Agronomique néo-Calédonien
- Location: Noumea, New Caledonia

Data management and analyses for the [CNRT BioIndic project](/projects/bioindic).

---
**Post-doc Bioinformatics & Data Science** From august 2017 to oct. 2018

- Company: IRD
- Location: UMR 9220 Entropie, Noumea, New Caledonia

Bioinformatic analyses of metatranscriptomic data for the [ANR CARIOCA](/projects/carioca).

---
**Post-doc Bioinformatic & Data Science** From nov. 2016 to juin 2017

- Company: University of Avignon
- Location Avignon, France

Lactuca genome comparisons in different postharvest conditions for [Lactuca](/projects/lactuca).

---
**Post-doc Data analysis & Data Science** From nov. 2015 to sept. 2016

- Company: University of Avignon
- Location Avignon, France

Projects:
- [CASDAR INNOHORT](/projects/innoraisin)
- [UV boosting](/projects/claranor)

Study of plant and fruit metabolisms responses to abiotic stress (water stress, elicitors, UV and light) and biotic stress (_botrytis cinerea_, _phytophthora infestans_).

---
**PhD student Ecophysiology & Data Science** From oct. 2011 to apr. 2015

- Company: INRAE UR1115 PSH & University of Avignon
- Location: Avignon, France

"Effects of water stress, only or in interaction with a pathogen, on plant functioning and fruit quality of _Solanum lycopersicum_ L., depending on genetic variation" [Thesis](/projects/thesis)

---
**Internship engineer** From feb. 2010 to sept. 2010

- Company: USDA-ARS European Biological Control Laboratory
- Location: Montpellier, France

"Contribution of genetic and life history traits to the invasive power of Sylverleaf nightshade as part of a biological control program" [Solanum elaeagnifolium](/projects/solanum-elaeagnifolium)

---
**Internship engineer** From feb. 2009 to july 2009

- Company: CIRAD
- Location: Montpellier, France & Kribi, Cameroon

"Interactions between the inflorescences of the oil palm _Elaeis guineensis_ J. and their pollinators of the genus _Elaeidobius spp._ F. (_Coleoptera_: _Curculionidae_) in Central Africa" [Elaeidobius](/projects/elaeidobius)

---
### Monitoring / Teaching

- Since 2024: Acadomia, tutoring in computer science
- From 2019 to 2021: Professional formations for researchers & companies with <a href="https://cnrsformation.cnrs.fr/liste-stages-130-Bioinformatique.html" target="_blank">CNRS Formation</a>: Bioinformatique pour le traitement de données de séquençage (NGS)
- Since 2014: Trainee frames (BTS to PhD student) & engineers
- Since 2014: R, Python, FAIR principles, Snakemake, Conda and statistics analysis trainings in consulting
- From 2011 to 2014: Teaching at University (Vegetal physiology, Initiation to laboratory working, Introduction to R, Climate Change adaptation)
