---
layout: default
title: Package dipwmsearch
subtitle: Internship Marie Mille 2021
---

### dipwmsearch: a python package for searching di-PWM motifs

- Research associates 2019-2021
- Institutes:
  - LIRMM, Montpellier, France

<div class="logos">
    <ul><li><img src="/assets/img/logo/lirmm.png" alt="logo LIRMM" /></li></ul>
</div>


<div class="mosaic">
    <img src="/assets/img/dipwm/dipwm_strat.png" class= "full" />
</div>


**Team members & co-workers**

- Marie Mille (LIRMM-UM)
- Rivals Eric (LIRMM-CNRS)
- Bastien Cazaux (LIRMM)

### Abstract

Motivation

Seeking probabilistic motifs in a sequence is a common task to annotate putative transcription factor binding sites or other RNA/DNA binding sites. Useful motif representations include position weight matrices (PWMs), dinucleotide PWMs (di-PWMs), and hidden Markov models (HMMs). Dinucleotide PWMs not only combine the simplicity of PWMs—a matrix form and a cumulative scoring function—but also incorporate dependency between adjacent positions in the motif (unlike PWMs which disregard any dependency). For instance to represent binding sites, the HOCOMOCO database provides di-PWM motifs derived from experimental data. Currently, two programs, SPRy-SARUS and MOODS, can search for occurrences of di-PWMs in sequences.

Results

We propose a Python package called dipwmsearch, which provides an original and efficient algorithm for this task (it first enumerates matching words for the di-PWM, and then searches these all at once in the sequence, even if the latter contains IUPAC codes). The user benefits from an easy installation via Pypi or conda, a comprehensive documentation, and executable scripts that facilitate the use of di-PWMs.

Availability and implementation

dipwmsearch is available at https://pypi.org/project/dipwmsearch/ and https://gite.lirmm.fr/rivals/dipwmsearch/ under Cecill license.

Keywords <mark>Bioinformatics</mark> <mark>diPWM</mark> <mark>Motif</mark> <mark>HOCOMOCO</mark>

### References

- Mille M., **Ripoll J.**, Cazaux B., Rivals E. <mark>2023</mark> Dipwmsearch: a python package for searching di-PWM motifs. Bioinformatics 39(4). <a href="http://dx.doi.org/10.1093/bioinformatics/btad141" target="_blank">DOI</a>

- Mille M., Ripoll J., Cazaux B., Rivals E. <mark>2021</mark>. Algorithms for searching dinucleotidic Position Weight Matrices (di-PWM). SeqBIM 2021, **Lyon, France** (November 25-26).

- Mille M., Ripoll J., Cazaux B., Rivals E. <mark>2021</mark>. Search algorithms for dinucleotide position weight matrices. JOBIM 2021,**Paris/Montpellier, France** (July 6-9). <a href="https://hal-lirmm.ccsd.cnrs.fr/MAB/lirmm-03442434v1" target="_blank">DOI</a>

- Mille M. <mark>2021</mark>. Recherche de motifs probabilistes : le cas des Matrices Poids Position dinucléotidiques (di-PWM). Rappport de stage de Master 1. [Rapport de recherche] Université de Montpellier. <a href="https://hal-lirmm.ccsd.cnrs.fr/lirmm-03326344/file/Mille_Marie_Rapport_Stage_M1_hal.pdf" target="_blank">DOI</a>
