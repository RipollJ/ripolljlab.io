- Ripoll J., Mandier C., Chen F., Rivals E.  **2023**. Joint transcriptome and translatome analysis: a reproducible pipeline. JOBIM 2023, **Nice, France** (June 27-30). <a href="https://jobim2023.sciencesconf.org/data/pages/proceedings.pdf" target="_blank">DOI</a>

- Carriconde F., Maggia L., Stokes A., Gardes M., Dinh K., Fernandez N., Demenois J., Stenger P.L., Ripoll J., Read J., Mournet P. **2023**. Nancy : ISES, 1 p.. International Conference on Serpentine Ecology (ICSE 2023). **Nancy, France** (June 12-16). <a href="https://publications.cirad.fr/une_notice.php?dk=605101" target="_blank">DOI</a>

- Mille M., Ripoll J., Cazaux B., Rivals E. **2021**. Algorithms for searching dinucleotidic Position Weight Matrices (di-PWM). SeqBIM 2021, **Lyon, France** (November 25-26).

- Aliaga B., Ripoll J., Chanal M., Hernandez-Varguaz H., Jouanneau E., Vasijevic A., Raverot G., Ribals E. Bertolino P. **2021**. Exploring the transcriptional landscape of gonadotroph tumor microenvironment with single cell RNA-seq. JOBIM 2021, **Paris/Montpellier, France** (July 6-9). <a href="https://hal-lirmm.ccsd.cnrs.fr/MAB/lirmm-03544671v1" target="_blank">DOI</a>

- Mille M., Ripoll J., Cazaux B., Rivals E. **2021**. Search algorithms for dinucleotide position weight matrices. JOBIM 2021,**Paris/Montpellier, France** (July 6-9). <a href="https://hal-lirmm.ccsd.cnrs.fr/MAB/lirmm-03442434v1" target="_blank">DOI</a>

- Lecompte F., Nicot PC., Ripoll J. et al **2016**. Exploration of constitutive and induced markers of disease severity caused by Botrytis cinerea in the primary metabolome of tomato. 17th International Botrytis Symposium, **Avignon, France** (October 23). <a href="https://hal.inrae.fr/hal-01594722" target="_blank">DOI</a>

- Frérot B., Beaudoin-Ollivier L., Ripoll J., Meyobeme H. **2010**. Mutualisme et pollinisation par duperie olfactive : Cas du palmier à huile et des Elaeidobius. Oral communication, 16ème colloque sur la biologie de l'insecte, **Lyon, France** (October 18-20). 

- Frérot B., Beaudoin-Ollivier L., Ripoll J., Meyobeme H. **2010**. Deception pollination in oil palm tree. Oral communication, PALMS 2010 International Symposium, **Montpellier, France** (May 5-7)
