---
layout: default
title: Lactuca
subtitle: Lactuca sativa post-harvest quality
---

### Quality and senescence of Lactuca sativa L.: a molecular study for a better understanding of plant physiological response during post-harvest storage

- Post-doctorate 2016-2017
- Institutes:
  - University UAPV, Avignon, France

<div class="logos">
    <ul>
        <li><img src="/assets/img/logo/uapv.png" alt="logo UAPV" /></li>
   </ul>
</div>


<div class="mosaic">
    <img src="/assets/img/lactuca/cut_day2_short.jpg" class="tall"/>
    <img src="/assets/img/lactuca/browning.jpg" />
    <img src="/assets/img/lactuca/lactuca.jpg" />
    <img src="/assets/img/lactuca/pathway_lactuca.jpg" class="wide"/>
</div>


**Team members & co-workers**

- Roux David (UAPV)
- Vidal Véronique (UAPV)
- Charles Florence (UAPV)
- Sallanon Huguette (UAPV)
- Laurent Sandrine (UAPV)
- Lopez-Lauri Félicie (UAPV)

### Part I: Transcriptomic view of detached lettuce leaves during storage: A crosstalk between wounding, dehydration and senescence

Many methods of storage are currently used to delay the postharvest senescence and the ensuing quality losses of detached lettuce leaves. This transcriptome study explores the senescence mechanisms of packaged lettuce leaves over 7 d of cold storage in darkness. At the end of the storage, the detached lettuce leaves showed a 10% water loss and 24% cut-edge discoloration. No chlorophyll fluorescence variation was observed. A total of 1048 and 1846 genes were differentially expressed (DE) after 2 and 7 d of storage, respectively. In terms of gene expression modulation, the data showed a global shutdown of primary-metabolism but no obvious chlorophyll breakdown. Although an early stress-response clearly takes place, the cold storage under darkness appeared to delay senescence by protecting chloroplasts from degradation and enhancing an antioxidative response. Then, a progressive global expression shutdown appeared, concomitantly with the dehydration stress. Among the transcripts that showed an early high expression, the data highlighted many stress-related genes (PIN, bzip TF, Heat-shock, stress A-like), 30 redox-regulation associated transcripts, the majority of the phenylpropanoid pathway markers and ethylene and auxin-related genes. Later (day 7), many dehydration-responsive markers showed an increased expression, as did some abscisic acid related markers. Phytohormones (cytokinins) and transcriptions factors (WRKYs) appeared to play complex roles as senescence progressed. No simple link between cut-edge discoloration and PPO-related genes expression was made. The main known process of senescence induction through the phaephorbide a oxygenase and phospholipase D pathways were not highlighted in this study. To the best of our knowledge, this study is the first to explore the transcriptional response of postharvest senescence in detached lettuce leaves. Deeper analysis of several markers will help to dissect the crosstalk between wounding, dehydration and senescence mechanisms. In addition, post-transcriptional studies are needed to conclude about the described patterns.

### Part II: Transcriptomic response of lettuce (Lactuca sativa L.) leaves to a postharvest intermittent-light treatment during storage

The quality of ready-to-eat lettuce strongly depends on storage conditions. Diverse light cycle treatments have been suggested to maintain quality. We have recently shown that low-level intermittent light could help to preserve the quality of packaged lettuce leaves. This work studied the effect of a two-day intermittent-light treatment (2 h on, 2 h off; 50 μmol m-2 s-1)followed by 5 days of dark storage, on packaged lettuce leaves at the transcriptome level. It evaluated impact of light on photosynthesis, carbon storage, membrane maintenance and lipids degradation related genes. The transcriptomic data highlighted the induction of carbon metabolism in chloroplast-related genes only during the two days of intermittent light, but this was not the case at day 7. Concerning membrane maintenance and lipids degradation, at day 2, some genes were differentially expressed, but unclear results avoid interpretation. At day 7 (after intermittent light treatment), most genes related to membrane maintenance were downregulated suggesting a negative remaining effect of light. The number of lipid degradation genes upregulated were equal at those downregulated at day 7. Some starch metabolism (anabolism and catabolism) related genes were upregulated at day 2, but not at day 7. This set of data are a resource for teams studying the postharvest behaviour of plant leaves.

Keywords <mark>Lactuca sativa L.</mark> <mark>Ecophysiology</mark> <mark>Bioinformatics</mark> <mark>Metabolisms</mark> <mark>Light treatment</mark> <mark>Transcriptome</mark> <mark>Post-harvest</mark> <mark>Senescence</mark>

### References

- Baron T., **Ripoll J.**, Dresch C., Charles F., Vidal V., Djari A., Maza E., Laurent S., Chervin C., Sallanon H. <mark>2023</mark> Transcriptomic response of lettuce ( Lactuca sativa L.) leaves to a postharvest intermittent-light treatment during storage. Acta Horticulturae 1364. XXXI International Horticultural Congress (IHC2022): International Symposium on Postharvest Technologies to Reduce Food Losses. <a href="http://dx.doi.org/10.17660/ActaHortic.2023.1364.18" target="_blank">DOI</a>

- **Ripoll J.**, Charles F., Vidal V., Laurent S., Klopp C., Laurie F., Sallanon H., Roux D. <mark>2019</mark>. Transcriptomic view of detached lettuce leaves during storage: A crosstalk between wounding, dehydration and senescence. Postharvest Biology and Technology, 152: 73-88. <a href="https://doi.org/10.1016/j.postharvbio.2019.02.004" target="_blank">DOI</a>
