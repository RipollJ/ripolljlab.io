---
layout: default
title: INCa FluoRib
subtitle: Ribosome heterogeneity and chemotherapy
---

### Alteration of ribosome function upon 5-fluorouracil treatment favors cancer cell drug-tolerance

- Post-doctorate 2022-2023
- Institutes:
  - LIRMM, Montpellier, France

<div class="logos">
    <ul><li><img src="/assets/img/logo/lirmm.png" alt="logo LIRMM" /></li></ul>
</div>

<div class="mosaic">
    <img src="/assets/img/5fu/5fu_treat.png" />
    <img src="/assets/img/5fu/5fu_go.png" />
    <img src="/assets/img/5fu/5fu_dge.png" class="full" />
</div>


**Team members & co-workers**

- Frédéric Catez (CRCL)
- Rivals Eric (LIRMM-CNRS)
- Alexandre David (IGF-CNRS)
- Jean-Jacques Diaz (CRCL)

### Abstract

Mechanisms of drug-tolerance remain poorly understood and have been linked to genomic but also to non-genomic processes. 5-fluorouracil (5-FU), the most widely used chemotherapy in oncology is associated with resistance. While prescribed as an inhibitor of DNA replication, 5-FU alters all RNA pathways. 

Here, we show that 5-FU treatment leads to the production of fluorinated ribosomes exhibiting altered translational activities. 5-FU is incorporated into ribosomal RNAs of mature ribosomes in cancer cell lines, colorectal xenografts, and human tumors. Fluorinated ribosomes appear to be functional, yet, they display a selective translational activity towards mRNAs depending on the nature of their 5′-untranslated region. 

As a result, we find that sustained translation of IGF-1R mRNA, which encodes one of the most potent cell survival effectors, promotes the survival of 5-FU-treated colorectal cancer cells. Altogether, our results demonstrate that “man-made” fluorinated ribosomes favor the drug-tolerant cellular phenotype by promoting translation of survival genes.

Keywords <mark>Bioinformatics</mark> <mark>Translatome</mark> <mark>Transcriptome</mark> <mark>Oncology</mark> <mark>5-FU</mark>

### References

- Therizols G., Bash-Imam Z., Panthu B., Machon C., Vincent A., **Ripoll J.**, et al. <mark>2022</mark>. Alteration of ribosome function upon 5-fluorouracil treatment favors cancer cell drug-tolerance. Nature Communications 13, 173. <a href="https://doi.org/10.1038/s41467-021-27847-8" target="_blank">DOI</a>

- Therizols G. et al. <mark>2023</mark>. Alteration of ribosome function upon 5-fluorouracil treatment
favors cancer cell drug-tolerance. JOBIM 2023, **Nice, France** (June 26-30). <a href="https://jobim2023.sciencesconf.org/data/pages/proceedings.pdf" target="_blank">DOI</a>
